'use strict'
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
var xmpp = require('node-xmpp')
var debug = require('debug')('e:events')
var config = require('app-config')
// var dateFormat = require('dateformat')
var sleep = require('system-sleep')
var cron = require('node-cron')
const JWT = require('jsonwebtoken')
var date = new Date()

// Set node-xmpp options.
// Replace with your projectID in the jid and your API key in the password
// The key settings for CCS are the last two to force SSL and Plain SASL auth.
var options = {
  type: 'client',
  port: 5235,
  host: 'gcm.googleapis.com',
  legacySSL: true,
  preferredSaslMechanism: 'PLAIN'
}
var procOS
var iosLimit = 10
var andLimit = 1
var processexit = false
var month = (date.getMonth() + 1)
var year = date.getFullYear()
var dayOfMonth = date.getDate()

console.log('month =', month)
console.log('year=', year)
console.log('dayOfMonth=', dayOfMonth)

var mongoConn
var db
var marr

var mongooptions = {
  auto_reconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 1000, // Maintain up to 1000 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  useNewUrlParser: true,
  promiseLibrary: require('bluebird')
}

var userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
var daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year

var authAndGetCol = function (marr, start, aagccb) {
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (!mongoConn) {
    mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
      if (err) {
        console.log('error while connecting mongoose', err)
      } else {
        console.log('connection successful')
        mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
          if (err) {
            console.log('error while connecting mongoose', err)
          } else {
            console.log('Mongoose: Connected')
            mongoConn = db
            authAndGetCol(marr, start, aagccb)
          }
        })
      }
    })
  } else {
    parseGCMMessage(marr, start, function (err, pgmrs) {
      aagccb(err, pgmrs)
    })
  }
}

var connectMongo = function () {
  marr = []
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()

  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (process.argv[2]) {
    procOS = process.argv[2]
    console.log('process os =', procOS)
    if (procOS === 'ios' || procOS === 'android') {
      if (procOS === 'ios') {
        options.jid = config.properties.config.iosId
        options.password = config.properties.config.iosKey
      } else if (procOS === 'android') {
        options.jid = config.properties.config.andId
        options.password = config.properties.config.andKey
      }
      if (config.properties.config.iosLimit) {
        iosLimit = config.properties.config.iosLimit
      }
      if (config.properties.config.andLimit) {
        andLimit = config.properties.config.andLimit
      }
      console.log('xmpp options', options)
      db = mongoose.connection

      db.once('error', function (err) {
        console.error('mongoose connection error' + err)
        mongoose.disconnect()
      })
      db.on('open', function () {
        console.log('successfully connected to mongoose')
      })
      db.on('reconnected', function () {
        console.log('MongoDB reconnected!')
      })
      db.on('disconnected', function () {
        console.log('MongoDB disconnected!')
        if (!processexit) {
          mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
            if (err) {
              console.log('error while connecting mongoose', err)
            } else {
              console.log('connection successful')
            }
          })
        }
      })
      mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
        if (err) {
          console.log('error while connecting mongoose', err)
        } else {
          console.log('Mongoose: Connected')
          mongoConn = db
          createIndex()
          initateXMPP()
        }
      })
    } else {
      console.log('invalid process language name', process.argv[2])
    }
  } else {
    console.log('required argument is not given')
  }
}

if (procOS === 'android') {
  console.log('index create cron started')
  // this task will run every day 12:05am
  cron.schedule('0 5 0 * * *', function () {
    createIndex()
  })
}

var processed = 0
var skipped = 0
var total = 0
var threads = 0
var respth = 0
var totalsleep = 0

const initateXMPP = function () {
  console.log('creating xmpp app')
  let cl = new xmpp.Client(options)
  cl.on('online', function () {
    console.log('online')

    // cl.connection.socket.setTimeout(0);
    // cl.connection.socket.setKeepAlive(true, 10000);
    cl.on('stanza', function (stanza) {
      // Best to ignore an error

      if (stanza.is('message') && stanza.attrs.type !== 'error') {
        console.log('Message received', marr.length)

        // Message format as per here: https://developer.android.com/google/gcm/ccs.html#upstream
        let messageData = JSON.parse(stanza.getChildText('gcm'))

        if (messageData && messageData.message_type !== 'ack' && messageData.message_type !== 'nack') {
          debug('**********messageData.data***********')
          debug(messageData)
          console.log('message::::::::::', messageData.data)
          marr.push(messageData.data)
          // marr.push(messageData.data);
          if (procOS === 'ios') {
            if (marr.length >= iosLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) console.error(err)
              })
              marr = []
            }
          } else {
            if (marr.length >= andLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) console.error(err)
              })
              marr = []
            }
          }

          debug('**********messageData.data***********')
          let ackMsg = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
            'to': messageData.from,
            'message_id': messageData.message_id,
            'message_type': 'ack'
          }))

          // send back the ack.
          cl.send(ackMsg)
          debug('Sent ack')

          // Now do something useful here with the message
          // e.g. awesomefunction(messageData);
          // but let's just log it.
          // debug(messageData);
        } else {
          // Need to do something more here for a nack.
          console.log('message was an ack or nack...discarding')
        }
      } else {
        console.error('error')
        console.error(stanza)
      }
    })

    cl.on('offline', function () {
      console.error('Client is offline')
    })

    cl.on('connect', function () {
      console.log('Client is connected')
    })

    cl.on('reconnect', function () {
      console.error('Client reconnects …')
    })

    cl.on('disconnect', function (e) {
      console.error('Client is disconnected', cl.connection.reconnect, e)
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) console.error(err)
        clDisconnect()
      })
    })

    cl.on('error', function (e) {
      console.error('Error occured:')
      console.error(e)
      console.error(e.children)
      console.error('reinitiating xmpp server connection')
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) console.error(err)
        clDisconnect()
      })
    })
  })
}

const clDisconnect = function () {
  if ((total === (processed + skipped)) || totalsleep === 180) {
    console.error('client disconnected, exiting process')
    console.error('skipped ', skipped)
    console.error('processed ', processed)
    console.error('total ', total)
    console.error('threads ', threads)
    console.error('respth ', respth)
    console.error('totalsleep ', totalsleep)
    if (mongoConn) {
      mongoConn.close()
    }
    process.exit(1)
  } else {
    sleep(30000)
    totalsleep = totalsleep + 30
    clDisconnect()
  }
}

let createIndex = function () {
  console.error('create index called')

  let collection = mongoConn.collection(userDaywiseStats)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_loans')
  collection.createIndex({ date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection(daywiseLoans)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_stats')
  collection.createIndex({ date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
  collection = mongoConn.collection('unknown_clicks')
  collection.createIndex({ date: 1, App_Version: 1, EventName: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
}

let parseGCMMessage = function (marr, start, pgmcb) {
  try {
    // if (marr[start]) {
    //   let message = marr[start]
    //   let updateData = {}; let loanData = {}
    //   var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    //   today = new Date(today)
    //   today = new Date(today.getTime() - 19800000)
    //   if (message.Token !== '') {
    //     JWT.verify(message.Token, 'W@mon^^7))9eYYY$riniVa$aRR@mAnuJ@NNn', function (err, tokenData) {
    //       if (err) {
    //         console.log('Error while decrypt the token', err)
    //         console.log('message::::::::::::', message)
    //       } else {
    //         console.log('*********', tokenData)

    //         if (message.EventName === 'splash_screen') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 opens_count: 1
    //               }
    //             }, { upsert: true, new: true }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //                 let collectionn = mongoConn.collection(userDaywiseStats)
    //                 collectionn.distinct('user_id', { used_date: today / 1, App_Version: message.App_Version }, function (countError, count) {
    //                   if (countError) {
    //                     console.log(countError)
    //                   } else {
    //                     console.log(count.length)
    //                     let updateDataOne = {
    //                       $set: { unique_opens: count.length },
    //                       $inc: {
    //                         total_opens: 1

    //                       }
    //                     }
    //                     collectionn = mongoConn.collection('overall_day_stats')
    //                     collectionn.findOneAndUpdate({ date: today / 1, App_Version: message.App_Version },
    //                       updateDataOne, { upsert: true, new: false }, function (error, result) {
    //                         if (error) {
    //                           console.log('errr', error)
    //                         } else {
    //                           // console.log(result)
    //                         }
    //                       })
    //                   }
    //                 })
    //               }
    //             })
    //         } else if (message.EventName === 'sms_synch_api_calling') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 sms_sync_count: 1
    //               }
    //             },
    //             { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'paynear_called') {
    //           updateData = {
    //             $inc: {
    //               paynear_used: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 paynear_used: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'paymentpage_from_nearby') {
    //           updateData = {
    //             $inc: {
    //               paymentpage_from_nearby: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 paymentpage_from_nearby: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'qrcode_scan_called') {
    //           updateData = {
    //             $inc: {
    //               qrcode_scan_used: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 qrcode_scan_used: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'paymentpage_from_qrscan') {
    //           updateData = {
    //             $inc: {
    //               paymentpage_from_qrscan: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 paymentpage_from_qrscan: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'paymentpage_pay_called') {
    //           updateData = {
    //             $inc: {
    //               total_paymentpage_pay_called: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 total_paymentpage_pay_called: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'CompletPaymentDONE') {
    //           updateData = {
    //             $inc: {
    //               payments_success: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 transaction_success: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })// CompletPaymentDONE
    //         } else if (message.EventName === 'paymentpage_location_cancel') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 location_cancel: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'paymentpage_location_ok') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 location_ok: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'instamoji_initiate_payment_called') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 payment_initiated_users: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'instamoji_initiate_payment_retry_called') {
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 payment_initiate_retried_users: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'Instamoji_payment_success') {
    //           updateData = {
    //             $inc: {
    //               instamojo_payment_success: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 instamojo_payment_success: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'Instamoji_payment_retry') {
    //           updateData = {
    //             $inc: {
    //               instamojo_payment_retry: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 instamojo_payment_retry: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'payment_cancel') {
    //           updateData = {
    //             $inc: {
    //               instamojo_payment_cancel: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 instamojo_payment_cancel: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'Instamoji_payment_failed') {
    //           updateData = {
    //             $inc: {
    //               Instamojo_payment_failed: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 Instamojo_payment_failed: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'personal_loan_selected') {
    //           loanData = {
    //             $inc: {
    //               personal_loans_selected: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 personal_loans_selected: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'business_loan_selected') {
    //           loanData = {
    //             $inc: {
    //               business_loans_selected: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 business_loans_selected: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'loansrequirement_1st_step_personal') {
    //           loanData = {
    //             $inc: {
    //               loansrequirement_1st_step_personal: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 loansrequirement_1st_step_personal: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'personal_2nd_step_personal') {
    //           loanData = {
    //             $inc: {
    //               personal_2nd_step_personal: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 personal_2nd_step_personal: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'employment_3rd_step_personal') {
    //           loanData = {
    //             $inc: {
    //               employment_3rd_step_personal: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 employment_3rd_step_personal: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'address_4th_step_personal') {
    //           loanData = {
    //             $inc: {
    //               address_4th_step_personal: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 address_4th_step_personal: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'bankoffers_5th_step_personal') {
    //           loanData = {
    //             $inc: {
    //               bankoffers_5th_step_personal: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 bankoffers_5th_step_personal: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'businessloan_1_step_business') {
    //           loanData = {
    //             $inc: {
    //               businessloan_1_step_business: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 businessloan_1_step_business: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'personal_2nd_step_business') {
    //           loanData = {
    //             $inc: {
    //               personal_2nd_step_business: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 personal_2nd_step_business: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'professional_3rd_step_business') {
    //           loanData = {
    //             $inc: {
    //               profession_3rd_step_business: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 profession_3rd_step_business: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'address_4th_step_business') {
    //           loanData = {
    //             $inc: {
    //               address_4th_step_business: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 address_4th_step_business: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'bankoffers_5th_step_business') {
    //           loanData = {
    //             $inc: {
    //               bankoffers_5th_step_business: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 bankoffers_5th_step_business: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'personal_loan_applied') {
    //           updateData = {
    //             $inc: {
    //               personal_loan_applied: 1
    //             }
    //           }
    //           loanData = {
    //             $inc: {
    //               personal_loan_applied: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 personal_loan_applied: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'business_loan_applied') {
    //           updateData = {
    //             $inc: {
    //               business_loan_applied: 1
    //             }
    //           }
    //           loanData = {
    //             $inc: {
    //               business_loan_applied: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 business_loan_applied: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'ADHAR_IMAGE_REQUEST') {
    //           loanData = {
    //             $inc: {
    //               ADHAR_IMAGE_REQUEST: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 ADHAR_IMAGE_REQUEST: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'All_Documents_Uploaded') {
    //           updateData = {
    //             $inc: {
    //               All_Documents_Uploaded: 1
    //             }
    //           }
    //           loanData = {
    //             $inc: {
    //               All_Documents_Uploaded: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 All_Documents_Uploaded: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'Income_IMAGE_REQUEST') {
    //           loanData = {
    //             $inc: {
    //               Income_IMAGE_REQUEST: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 Income_IMAGE_REQUEST: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             }) //
    //         } else if (message.EventName === 'Address_IMAGE_REQUEST') {
    //           loanData = {
    //             $inc: {
    //               Address_IMAGE_REQUEST: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 Address_IMAGE_REQUEST: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'UploadDocuments_Page') {
    //           loanData = {
    //             $inc: {
    //               UploadDocuments_Page: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 UploadDocuments_Page: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             }) //
    //         } else if (message.EventName === 'uploaddocuments_skip_called') {
    //           loanData = {
    //             $inc: {
    //               UploadDocuments_skip_called: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 UploadDocuments_skip_called: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'loan_status_page') {
    //           loanData = {
    //             $inc: {
    //               loan_status_page: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(daywiseLoans)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 loan_status_page: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'dashboard_location_cancel') {
    //           updateData = {
    //             $inc: {
    //               location_cancel: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 location_cancel: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'dashboard_location_ok') {
    //           updateData = {
    //             $inc: {
    //               location_ok: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 location_ok: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'usage_history') {
    //           updateData = {
    //             $inc: {
    //               usage_history: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 usage_history: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'UsageHistory_paidHistroy') {
    //           updateData = {
    //             $inc: {
    //               UsageHistory_paidHistroy: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 UsageHistory_paidHistroy: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'UsageHistory_usedHistroy') {
    //           updateData = {
    //             $inc: {
    //               UsageHistory_usedHistroy: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 UsageHistory_usedHistroy: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'Location_Error_NearBy') {
    //           updateData = {
    //             $inc: {
    //               location_error: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 location_error: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'offers') {
    //           updateData = {
    //             $inc: {
    //               offers: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 offers: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'camera_permission_denied') {
    //           updateData = {
    //             $inc: {
    //               camera_permission_denied: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 camera_permission_denied: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'camera_permission_allowed') {
    //           updateData = {
    //             $inc: {
    //               camera_permission_allowed: 1
    //             }
    //           }
    //           let collection = mongoConn.collection(userDaywiseStats)
    //           collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version },
    //             {
    //               $inc: {
    //                 camera_permission_allowed: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         } else if (message.EventName === 'menu_logout_called' || message.EventName === 'bottom_navigation_loans_called' || message.EventName === 'SMS_Synch_API_Calling' || message.EventName === 'Total_SMS_SYNCH' ||
    //           message.EventName === 'profile_update_called_from_edit' || message.EventName === 'profile_update' || message.EventName === 'sms_subid_notfound' || message.EventName === 'otp_verification_page' ||
    //           message.EventName === 'bottom_navigation_loans_called' || message.EventName === 'bottom_navigation_expense_called' || message.EventName === 'bottom_navigation_credits_called' || message.EventName === 'bottom_navigation_payments_called') {
    //           processed = processed + 1
    //           parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
    //         } else {
    //           // var EventName = message.EventName
    //           let collection = mongoConn.collection('unknown_clicks')
    //           collection.findOneAndUpdate({ date: today / 1, App_Version: message.App_Version, EventName: message.EventName },
    //             {
    //               $inc: {
    //                 count: 1
    //               }
    //             }, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //                 processed = processed + 1
    //                 parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
    //               }
    //             })
    //         }
    //         if (Object.getOwnPropertyNames(updateData).length > 0) {
    //           let collection = mongoConn.collection('overall_day_stats')
    //           collection.findOneAndUpdate({ date: today / 1, App_Version: message.App_Version },
    //             updateData, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         }
    //         if (Object.getOwnPropertyNames(loanData).length > 0) {
    //           let collection = mongoConn.collection('overall_day_loans')
    //           collection.findOneAndUpdate({ date: today / 1, App_Version: message.App_Version },
    //             loanData, { upsert: true, new: false }, function (error, result) {
    //               if (error) {
    //                 console.log('errr', error)
    //               } else {
    //                 // console.log(result)
    //               }
    //             })
    //         }
    //       }
    //     })
    //   } else {
    //     skipped = skipped + 1
    //     console.log('invalid message', message)
    //     parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
    //   }
    // } else {
    //   respth = respth + 1
    //   console.log('skipped ', skipped)
    //   console.log('processed ', processed)
    //   console.log('total ', total)
    //   console.log('threads ', threads)
    //   console.log('respth ', respth)
    //   pgmcb(null, 'Finished')
    // }
    if (marr[start]) {
      let message = marr[start]
      // let updateData = {}
      // let loanData = {}
      // var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      // today = new Date(today)
      // today = new Date(today.getTime() - 19800000)
      let collection = mongoConn.collection('unknown_clicks')
      if (message.Token !== '') {
        JWT.verify(message.Token, 'W@mon^^7))9eYYY$riniVa$aRR@mAnuJ@NNn', function (err, tokenData) {
          if (err) console.error(err)
          if (tokenData) {
            collection.insertOne({ date: new Date(), App_Version: message.App_Version, mobile_number: message.Number, user_id: tokenData._id, EventName: message.EventName }, function (error, result) {
              if (error) {
                console.log('errr', error)
              } else {
                // console.log(result)
                processed = processed + 1
              }
              parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
            })
          }
        })
      } else {
        collection.insertOne({ date: new Date(), App_Version: message.App_Version, mobile_number: message.Number, user_id: '', EventName: message.EventName }, function (error, result) {
          if (error) {
            console.log('errr', error)
          } else {
            // console.log(result)
            processed = processed + 1
          }
          parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
        })
      }
    } else {
      parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
    }
  } catch (cerr) {
    console.error('unkown error occured in parseGCMMessage')
    console.error(cerr)
    parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
  }
}

connectMongo()

process.on('uncaughtException', function (err) {
  console.error((new Date()).toUTCString() + ' uncaughtException:', err.message)
  console.error(err)
})

// process.on('SIGINT', function () {
//   console.log(' on exit called by node')
//   processexit = true
//   mongoose.disconnect()
//   db.close()
//   process.exit(1)
// })
