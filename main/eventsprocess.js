'use strict'
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
var xmpp = require('node-xmpp')
var debug = require('debug')('e:events')
var config = require('app-config')
var sleep = require('system-sleep')
var cron = require('node-cron')
const JWT = require('jsonwebtoken')
var date = new Date()
var dateFormat = require('dateformat')

// Set node-xmpp options.
// Replace with your projectID in the jid and your API key in the password
// The key settings for CCS are the last two to force SSL and Plain SASL auth.
var options = {
  type: 'client',
  port: 5235,
  host: 'gcm.googleapis.com',
  legacySSL: true,
  preferredSaslMechanism: 'PLAIN'
}
var procOs
var iosLimit = 10
var andLimit = 10

var month = (date.getMonth() + 1)
var year = date.getFullYear()
var dayOfMonth = date.getDate()

console.log('month =', month)
console.log('year=', year)
console.log('dayOfMonth=', dayOfMonth)

var mongoConn
connectMongo()
var marr

var mongooptions = {
  auto_reconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 1000, // Maintain up to 1000 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  useNewUrlParser: true,
  promiseLibrary: require('bluebird')
}

var userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
var daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year

function connectMongo () {
  marr = []
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()

  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (process.argv[2]) {
    procOs = process.argv[2]
    console.log('process os =', procOs)
    if (procOs === 'ios' || procOs === 'android') {
      if (procOs === 'ios') {
        options.jid = config.properties.config.iosId
        options.password = config.properties.config.iosKey
      } else if (procOs === 'android') {
        options.jid = config.properties.config.andId
        options.password = config.properties.config.andKey
      }
      if (config.properties.config.iosLimit) {
        iosLimit = config.properties.config.iosLimit
      }
      if (config.properties.config.andLimit) {
        andLimit = config.properties.config.andLimit
      }
      console.log('xmpp options', options)
      const db = mongoose.connection

      db.once('error', function (err) {
        console.error('mongoose connection error' + err)
        mongoose.disconnect()
      })
      db.on('open', function () {
        console.log('successfully connected to mongoose')
      })
      db.on('reconnected', function () {
        console.log('MongoDB reconnected!')
      })
      db.on('disconnected', function () {
        console.log('MongoDB disconnected!')

        mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
          if (err) {
            console.log('error while connecting mongoose', err)
          } else {
            console.log('connection successful')
          }
        })
      })
      mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
        if (err) {
          console.log('error while connecting mongoose', err)
        } else {
          console.log('Mongoose: Connected')
          mongoConn = db
          createIndex()
          initateXMPP()
        }
      })
    } else {
      console.log('invalid process language name', process.argv[2])
    }
  } else {
    console.log('required argument is not given')
  }
}

if (procOs === 'android') {
  console.log('index create cron started')
  // this task will run every day 12:05am
  cron.schedule('0 5 0 * * *', function () {
    // createIndex();
  })
}

var processed = 0
var skipped = 0
var total = 0
var threads = 0
var respth = 0
var totalsleep = 0

function initateXMPP () {
  console.log('creating xmpp app')
  let cl = new xmpp.Client(options)
  console.log(cl)
  cl.on('online', function () {
    console.log('online')

    // cl.connection.socket.setTimeout(0);
    // cl.connection.socket.setKeepAlive(true, 10000);
    cl.on('stanza', function (stanza) {
      // Best to ignore an error

      if (stanza.is('message') && stanza.attrs.type !== 'error') {
        console.log('Message received', marr.length)

        // Message format as per here: https://developer.android.com/google/gcm/ccs.html#upstream
        let messageData = JSON.parse(stanza.getChildText('gcm'))

        if (messageData && messageData.message_type !== 'ack' && messageData.message_type !== 'nack') {
          debug('**********messageData.data***********')
          debug(messageData)
          console.log('message::::::::::', messageData.data)
          marr.push(messageData.data)
          // marr.push(messageData.data);
          if (procOs === 'ios') {
            if (marr.length >= iosLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) {
                  throw err
                }
              })
              marr = []
            }
          } else {
            if (marr.length >= andLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) {
                  console.log(err)
                }
              })
              marr = []
            }
          }

          debug('**********messageData.data***********')
          let ackMsg = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
            'to': messageData.from,
            'message_id': messageData.message_id,
            'message_type': 'ack'
          }))

          // send back the ack.
          cl.send(ackMsg)
          debug('Sent ack')

          // Now do something useful here with the message
          // e.g. awesomefunction(messageData);
          // but let's just log it.
          // debug(messageData);
        } else {
          // Need to do something more here for a nack.
          console.log('message was an ack or nack...discarding')
        }
      } else {
        console.error('error')
        console.error(stanza)
      }
    })

    cl.on('offline', function () {
      console.error('Client is offline')
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        } else {
          if (mongoConn) {
            mongoConn.close()
          }
          // clDisconnect()
          process.exit(1)
        }
      })
    })

    cl.on('connect', function () {
      console.log('Client is connected')
    })

    cl.on('reconnect', function () {
      console.error('Client reconnects …')
    })

    cl.on('disconnect', function (e) {
      console.error('Client is disconnected', cl.connection.reconnect, e)
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        }
        if (mongoConn) {
          mongoConn.close()
        }
        process.exit(1)
        // clDisconnect()
      })
    })

    cl.on('error', function (e) {
      console.error('Error occured:')
      console.error(e)
      console.error(e.children)
      console.error('reinitiating xmpp server connection')
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        } else {
          if (mongoConn) {
            mongoConn.close()
          }
          process.exit(1)
          // clDisconnect()
        }
      })
    })
  })
}

function clDisconnect () {
  if ((total === (processed + skipped)) || totalsleep === 180) {
    console.error('client disconnected, exiting process')
    console.error('skipped ', skipped)
    console.error('processed ', processed)
    console.error('total ', total)
    console.error('threads ', threads)
    console.error('respth ', respth)
    console.error('totalsleep ', totalsleep)
    if (mongoConn) {
      mongoConn.close()
    }
  } else {
    sleep(30000)
    totalsleep = totalsleep + 30
    clDisconnect()
  }
}

let authAndGetCol = function (marr, start, aagccb) {
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (!mongoConn) {
    connectMongo(function (err, ccrs) {
      if (err) {
        aagccb(err, ccrs)
      } else {
        authAndGetCol(marr, start, aagccb)
      }
    })
  } else {
    parseGCMMessage(marr, start, function (err, pgmrs) {
      aagccb(err, pgmrs)
    })
  }
}

let createIndex = function () {
  console.error('create index called')

  let collection = mongoConn.collection(userDaywiseStats)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1, os: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_loans')
  collection.createIndex({ used_date: 1, App_Version: 1, os: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection(daywiseLoans)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1, os: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_stats')
  collection.createIndex({ used_date: 1, App_Version: 1, os: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
  collection = mongoConn.collection('unknown_clicks')
  collection.createIndex({ used_date: 1, App_Version: 1, EventName: 1, os: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
}
let parseGCMMessage = function (marr, start, pgmcb) {
  try {
    if (marr[start]) {
      let message = marr[start]; let sms = ''
      let updateData = {}; let loanData = {}; let uniqueData = {}
      date = new Date()
      month = date.getMonth() + 1
      year = date.getFullYear()
      dayOfMonth = date.getDate()
      userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
      daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
      var today = (new Date()).setHours(0, 0, 0, 0)
      if (message.Token !== '' || message.Token !== undefined) {
        JWT.verify(message.Token, 'W@mon^^7))9eYYY$riniVa$aRR@mAnuJ@NNn', function (err, tokenData) {
          if (err) {
            console.log('Error while decrypt the token', err)
            console.log('message::::::::::::', message)
          } else {
            console.log('*********', tokenData)

            if (message.EventName === 'splash_screen' && message.App_Version === '3.0') {
              // let collection = mongoConn.collection(userDaywiseStats)
              // collection.findOne({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version, os: process.argv[2] },
              //   function (error, resultData) {
              //     if (error) {
              //       console.log('errr', error)
              //     } else {
              updateData = {
                total_opens: 1,
                paylater_opens: 1
              }
              uniqueData = {
                unique_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              //   }
              // })
            } else if (message.EventName === 'dashboard_called') {
              updateData = {
                total_opens: 1,
                paylater_opens: 1
              }
              uniqueData = {
                unique_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'sms_synch_api_calling') {
              updateData = {
                sms_sync_count: 1
              }
              uniqueData = {
                unique_sms_sync_count: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paynear_called') {
              updateData = {
                paynear_opens: 1
              }
              uniqueData = {
                unique_paynear_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paymentpage_from_nearby') {
              updateData = {
                nearby_paymentpage: 1
              }
              uniqueData = {
                unique_nearby_paymentpage: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'qrcode_scan_called') {
              updateData = {
                qrcode_scan_opens: 1
              }
              uniqueData = {
                unique_qrcode_scan_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paymentpage_from_qrscan') {
              updateData = {
                qrscan_paymentpage: 1
              }
              uniqueData = {
                unique_qrscan_paymentpage: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'completpaymentdone') {
              updateData = {
                transaction_success: 1
              }
              uniqueData = {
                unique_transaction_success: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paymentpage_canceled') {
              updateData = {
                transaction_cancel: 1
              }
              uniqueData = {
                unique_transaction_cancel: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paymentpage_location_cancel') {
              updateData = { location_cancel: 1 }
              uniqueData = {
                unique_location_cancel: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'paymentpage_location_ok') {
              updateData = { location_ok: 1 }
              uniqueData = { unique_location_ok: 1 }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'instamoji_initiate_payment_called') {
              updateData = {
                paydue_opens: 1
              }
              uniqueData = {
                unique_paydue_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'payment_success') {
              updateData = {
                payment_success: 1
              }
              uniqueData = {
                unique_payment_success: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'instamoji_payment_retry' || message.EventName === 'instamoji_initiate_payment_retry_called') {
              updateData = {
                payment_retry: 1
              }
              uniqueData = {
                unique_payment_retry: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'payment_cancel') {
              updateData = {
                payment_cancel: 1
              }
              uniqueData = {
                unique_payment_cancel: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'instamoji_payment_failed') {
              updateData = {
                payment_failed: 1
              }
              uniqueData = {
                unique_payment_failed: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'personal_loan_selected') {
              loanData = {
                personal_loan_opens: 1
              }
              updateData = {
                personal_loan_opens: 1
              }
              uniqueData = {
                unique_personal_loan_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'business_loan_selected') {
              loanData = {
                business_loan_opens: 1
              }
              updateData = {
                business_loan_opens: 1
              }
              uniqueData = {
                unique_business_loan_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'loansrequirement_1st_step_personal') {
              loanData = {
                personalloan_1st_step: 1
              }
              uniqueData = {
                unique_personalloan_1st_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'personal_2nd_step_personal') {
              loanData = {
                personalloan_2nd_step: 1
              }
              uniqueData = {
                unique_personalloan_2nd_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'employment_3rd_step_personal') {
              loanData = {
                personalloan_3rd_step: 1
              }
              uniqueData = {
                unique_personalloan_3rd_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'address_4th_step_personal') {
              loanData = {
                personalloan_4th_step: 1
              }
              uniqueData = {
                unique_personalloan_4th_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bankoffers_5th_step_personal') {
              loanData = {
                personalloan_5th_step: 1
              }
              uniqueData = {
                unique_personalloan_5th_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'businessloan_1_step_business') {
              loanData = {
                businessloan_1st_step: 1
              }
              uniqueData = {
                unique_businessloan_1st_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'personal_2nd_step_business') {
              loanData = {
                businessloan_2nd_step: 1
              }
              uniqueData = {
                unique_businessloan_2nd_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'professional_3rd_step_business') {
              loanData = {
                businessloan_3rd_step: 1
              }
              uniqueData = {
                unique_businessloan_3rd_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'address_4th_step_business') {
              loanData = {
                businessloan_4th_step: 1
              }
              uniqueData = {
                unique_businessloan_4th_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bankoffers_5th_step_business') {
              loanData = {
                businessloan_5th_step: 1
              }
              uniqueData = {
                unique_businessloan_5th_step: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bank_offers_more_details_business') {
              loanData = {
                businessloan_bankoffers: 1
              }
              uniqueData = {
                unique_businessloan_bankoffers: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bank_offers_more_details_personal') {
              loanData = {
                personalloan_bankoffers: 1
              }
              uniqueData = {
                unique_personalloan_bankoffers: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'applybutton_clicked personal') {
              updateData = {
                personal_loan_applied: 1
              }
              loanData = {
                personal_loan_applied: 1
              }
              uniqueData = {
                unique_personal_loan_applied: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'applybutton_clicked business') {
              updateData = {
                business_loan_applied: 1
              }
              loanData = {
                business_loan_applied: 1
              }
              uniqueData = {
                unique_business_loan_applied: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'all_documents_uploaded') {
              updateData = {
                documents_uploaded: 1
              }
              loanData = {
                documents_uploaded: 1
              }
              uniqueData = {
                unique_documents_uploaded: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'address_image_request_personal') {
              loanData = {
                personal_loan_address_image: 1
              }

              uniqueData = {
                unique_personal_loan_address_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'address_image_request_business') {
              loanData = {
                business_loan_address_image: 1
              }
              uniqueData = {
                unique_business_loan_address_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'adhar_image_request_personal') {
              loanData = {
                personal_loan_aadhar_image: 1
              }
              uniqueData = {
                unique_personal_loan_aadhar_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'income_image_request_personal') {
              loanData = {
                personal_loan_income_image: 1
              }
              uniqueData = {
                unique_personal_loan_income_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'income_image_request_business') {
              loanData = {
                business_loan_income_image: 1
              }
              uniqueData = {
                unique_business_loan_income_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'adhar_image_request_business') {
              loanData = {
                business_loan_aadhar_image: 1
              }
              uniqueData = {
                unique_business_loan_aadhar_image: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'uploaddocuments_page_personal') {
              loanData = {
                personal_loan_documents_page: 1
              }
              uniqueData = {
                unique_personal_loan_documents_page: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'uploaddocuments_page_business') {
              loanData = {
                business_loan_documents_page: 1
              }
              uniqueData = {
                unique_business_loan_documents_page: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'uploaddocuments_skip_called') {
              loanData = {
                documents_skipped_users: 1
              }
              uniqueData = {
                unique_documents_skipped_users: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'loan_status_page_personal') {
              loanData = {
                personal_loan_status_page: 1
              }
              uniqueData = {
                unique_personal_loan_status_page: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'view loans history') {
              loanData = {
                loans_history: 1
              }
              uniqueData = {
                unique_loans_history: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'loan_status_page_business') {
              loanData = {
                business_loan_status_page: 1
              }
              uniqueData = {
                unique_business_loan_status_page: 1
              }
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'dashboard_location_cancel') {
              updateData = {
                location_cancel: 1
              }
              uniqueData = {
                unique_location_cancel: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'dashboard_location_ok') {
              updateData = {
                location_ok: 1
              }
              uniqueData = {
                unique_location_ok: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'usage_history' || message.EventName === 'usagehistory_usedhistroy') {
              updateData = {
                usage_history: 1
              }
              uniqueData = {
                unique_usage_history: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'usagehistory_paidhistroy') {
              updateData = {
                paid_histroy: 1
              }
              uniqueData = {
                unique_paid_histroy: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'location_error_nearby') {
              updateData = {
                location_error: 1
              }
              uniqueData = {
                unique_location_error: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'offers') {
              updateData = {
                offers_opens: 1
              }
              uniqueData = {
                unique_offers_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'camera_permission_denied') {
              uniqueData = {
                unique_camera_permission_denied: 1
              }
              updateData = {
                camera_permission_denied: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'camera_permission_allowed') {
              uniqueData = {
                unique_camera_permission_allowed: 1
              }
              updateData = {
                camera_permission_allowed: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bottom_navigation_loans_called') {
              loanData = {
                loan_opens: 1
              }
              updateData = {
                loan_opens: 1
              }
              uniqueData = {
                unique_loan_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              insertIntoLoanDb(tokenData._id, message.Number, message.App_Version, loanData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueLoansCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'menu_logout_called') {
              updateData = {
                logout_users: 1
              }
              uniqueData = {
                unique_logout_users: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'profile_update_called_from_edit') {
              updateData = {
                edit_profile: 1
              }
              uniqueData = {
                unique_edit_profile: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bottom_navigation_expense_called') {
              updateData = {
                expense_manager_opens: 1
              }
              uniqueData = {
                unique_expense_manager_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bottom_navigation_credits_called') {
              updateData = {
                credit_line_opens: 1
              }
              uniqueData = {
                unique_credit_line_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'bottom_navigation_payments_called') {
              updateData = {
                paylater_opens: 1
              }
              uniqueData = {
                unique_paylater_opens: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'menu_profile_called') {
              updateData = {
                view_profile: 1
              }
              uniqueData = {
                unique_view_profile: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'sms_subid_notfound') {
              updateData = {
                sms_subid_notfound: 1
              }
              uniqueData = {
                unique_sms_subid_notfound: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (message.EventName === 'email_change') {
              updateData = {
                email_change: 1
              }
              uniqueData = {
                unique_email_change: 1
              }
              insertIntoStatsDb(tokenData._id, message.Number, message.App_Version, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
              updateUniqueUsersCount(tokenData._id, message.Number, message.App_Version, uniqueData, updateData, function (error, resp) {
                if (error) {
                  console.log(error)
                }
              })
            } else if (
              message.EventName === 'paymentpage_pay_called_success' || message.EventName === 'expensemanager' || message.EventName === 'otp_verification_page' || (message.EventName === 'splash_screen' && message.App_Version !== '3.0')) {
              processed = processed + 1
              parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
            } else if (message.EventName) {
              sms = parseInt(message.EventName.replace('total_sms_synch', ''))
              if (sms !== '') {
                // console.log('in sms sync::::::::::', message.EventName, parseInt(sms), typeof sms)
                let collection = mongoConn.collection(userDaywiseStats)
                collection.findOneAndUpdate({ user_id: mongoose.Types.ObjectId(tokenData._id), mobile_number: message.Number, used_date: today / 1, App_Version: message.App_Version, os: process.argv[2] },
                  {
                    $inc: {
                      sms_count: sms
                    }
                  }, { upsert: true, new: false }, function (error, result) {
                    if (error) {
                      console.log('errr', error)
                    } else {
                      // console.log(result)
                    }
                  })
              }
            } else {
              let collection = mongoConn.collection('unknown_clicks')
              collection.findOneAndUpdate({ used_date: today / 1, App_Version: message.App_Version, EventName: message.EventName, os: process.argv[2] },
                {
                  $inc: {
                    count: 1
                  }
                }, { upsert: true, new: false }, function (error, result) {
                  if (error) {
                    console.log('errr', error)
                  } else {
                    // console.log(result)
                    processed = processed + 1
                    parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
                  }
                })
            }
          }
        })
      } else {
        skipped = skipped + 1
        console.log('invalid message', message)
        parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
      }
    } else {
      respth = respth + 1
      console.log('skipped ', skipped)
      console.log('processed ', processed)
      console.log('total ', total)
      console.log('threads ', threads)
      console.log('respth ', respth)
      pgmcb(null, 'Finished')
    }
  } catch (cerr) {
    console.error('unkown error occured in parseGCMMessage')
    console.error(cerr)
    parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
  }
}

let insertIntoStatsDb = function (userId, mobileNumber, appVersion, updateData, iidcd) {
  console.log(updateData)
  console.log('here')
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime() - 19800000)
  mongoConn.collection('overall_day_stats').findOneAndUpdate({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
    { $inc: updateData }, { upsert: true, new: false }, function (error, result) {
      if (error) {
        console.log('errr', error)
      } else {
        // console.log(result)
        mongoConn.collection(userDaywiseStats).findOneAndUpdate({ user_id: mongoose.Types.ObjectId(userId), mobile_number: mobileNumber, used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
          { $inc: updateData }, { upsert: true, new: false }, function (error, resultData) {
            if (error) {
              console.log('errr', error)
            } else {
            }
          })
      }
    })
}

let insertIntoLoanDb = function (userId, mobileNumber, appVersion, loanData, iiLcd) {
  console.log('here')
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime() - 19800000)
  mongoConn.collection('overall_day_loans').findOneAndUpdate({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
    { $inc: loanData }, { upsert: true, new: false }, function (error, result) {
      if (error) {
        console.log('errr', error)
      } else {
        // console.log(result)
        mongoConn.collection(daywiseLoans).findOneAndUpdate({ user_id: mongoose.Types.ObjectId(userId), mobile_number: mobileNumber, used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
          { $inc: loanData }, { upsert: true, new: false }, function (error, resultData) {
            if (error) {
              console.log('errr', error)
            } else {
            }
          })
      }
    })
}

let updateUniqueUsersCount = function (userId, mobileNumber, appVersion, uniqueData, updateData, uuccd) {
  console.log('unique  counts', Object.getOwnPropertyNames(updateData)[0])
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime() - 19800000)
  mongoConn.collection(userDaywiseStats).findOne({ user_id: mongoose.Types.ObjectId(userId), mobile_number: mobileNumber, used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
    function (error, resultData) {
      if (error) {
        console.log('errr', error)
      } else {
        if (resultData !== null && resultData !== undefined) {
          console.log(' in if', typeof resultData)
          if (resultData[Object.getOwnPropertyNames(updateData)[0]] === undefined) {
            console.log('result undefined::::::::::::', resultData)
            mongoConn.collection('overall_day_stats').findOneAndUpdate({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
              { $inc: uniqueData }, { upsert: true, new: false }, function (error, resultData) {
                if (error) {
                  console.log('errr', error)
                } else {
                }
              })
          }
        } else {
          mongoConn.collection('overall_day_stats').findOneAndUpdate({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
            { $inc: uniqueData }, { upsert: true, new: false }, function (error, resultData) {
              if (error) {
                console.log('errr', error)
              } else {
              }
            })
        }
      }
    })
}
let updateUniqueLoansCount = function (userId, mobileNumber, appVersion, uniqueData, updateData, uuccd) {
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime() - 19800000)
  mongoConn.collection(daywiseLoans).findOne({ user_id: mongoose.Types.ObjectId(userId), mobile_number: mobileNumber, used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
    function (error, resultData) {
      if (error) {
        console.log('errr', error)
      } else {
        if (resultData !== null && resultData !== undefined) {
          console.log(' in if', typeof resultData)
          if (resultData[Object.getOwnPropertyNames(updateData)[0]] === undefined) {
            mongoConn.collection('overall_day_loans').update({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
              { $inc: uniqueData }, { upsert: true, new: false }, function (error, resultData) {
                if (error) {
                  console.log('errr', error)
                } else {
                }
              })
          }
        } else {
          mongoConn.collection('overall_day_loans').update({ used_date: today / 1, App_Version: appVersion, os: process.argv[2] },
            { $inc: uniqueData }, function (error, resultData) {
              if (error) {
                console.log('errr', error)
              } else {
              }
            })
        }
      }
    })
}
