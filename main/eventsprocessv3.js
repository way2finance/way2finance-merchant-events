'use strict'
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
var xmpp = require('node-xmpp')
var debug = require('debug')('e:events')
var config = require('app-config')

var sleep = require('system-sleep')
var cron = require('node-cron')
const JWT = require('jsonwebtoken')
var date = new Date()

// Set node-xmpp options.
// Replace with your projectID in the jid and your API key in the password
// The key settings for CCS are the last two to force SSL and Plain SASL auth.
var options = {
  type: 'client',
  port: 5235,
  host: 'gcm.googleapis.com',
  legacySSL: true,
  preferredSaslMechanism: 'PLAIN'
}
var procOs
var iosLimit = 10
var andLimit = 10

var month = (date.getMonth() + 1)
var year = date.getFullYear()
var dayOfMonth = date.getDate()

console.log('month =', month)
console.log('year=', year)
console.log('dayOfMonth=', dayOfMonth)

var mongoConn
connectMongo()
var marr

var mongooptions = {
  auto_reconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 1000, // Maintain up to 1000 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  useNewUrlParser: true,
  promiseLibrary: require('bluebird')
}

var userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
var daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year

function connectMongo () {
  marr = []
  date = new Date()
  month = (date.getMonth() + 1)
  year = date.getFullYear()
  dayOfMonth = date.getDate()

  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (process.argv[2]) {
    procOs = process.argv[2]
    console.log('process os =', procOs)
    if (procOs === 'ios' || procOs === 'android') {
      if (procOs === 'ios') {
        options.jid = config.properties.config.iosId
        options.password = config.properties.config.iosKey
      } else if (procOs === 'android') {
        options.jid = config.properties.config.andId
        options.password = config.properties.config.andKey
      }
      if (config.properties.config.iosLimit) {
        iosLimit = config.properties.config.iosLimit
      }
      if (config.properties.config.andLimit) {
        andLimit = config.properties.config.andLimit
      }
      console.log('xmpp options', options)
      const db = mongoose.connection

      db.once('error', function (err) {
        console.error('mongoose connection error' + err)
        mongoose.disconnect()
      })
      db.on('open', function () {
        console.log('successfully connected to mongoose')
      })
      db.on('reconnected', function () {
        console.log('MongoDB reconnected!')
      })
      db.on('disconnected', function () {
        console.log('MongoDB disconnected!')

        mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
          if (err) {
            console.log('error while connecting mongoose', err)
          } else {
            console.log('connection successful')
          }
        })
      })
      mongoose.connect(config.properties.mongodb.uri, mongooptions, function (err, db) {
        if (err) {
          console.log('error while connecting mongoose', err)
        } else {
          console.log('Mongoose: Connected')
          mongoConn = db
          createIndex()
          initateXMPP()
        }
      })
    } else {
      console.log('invalid process language name', process.argv[2])
    }
  } else {
    console.log('required argument is not given')
  }
}

if (procOs === 'android') {
  console.log('index create cron started')
  // this task will run every day 12:05am
  cron.schedule('0 5 0 * * *', function () {
    // createIndex();
  })
}

var processed = 0
var skipped = 0
var total = 0
var threads = 0
var respth = 0
var totalsleep = 0

function initateXMPP () {
  console.log('creating xmpp app')
  let cl = new xmpp.Client(options)
  cl.on('online', function () {
    console.log('online')

    // cl.connection.socket.setTimeout(0);
    // cl.connection.socket.setKeepAlive(true, 10000);
    cl.on('stanza', function (stanza) {
      // Best to ignore an error

      if (stanza.is('message') && stanza.attrs.type !== 'error') {
        console.log('Message received', marr.length)

        // Message format as per here: https://developer.android.com/google/gcm/ccs.html#upstream
        let messageData = JSON.parse(stanza.getChildText('gcm'))

        if (messageData && messageData.message_type !== 'ack' && messageData.message_type !== 'nack') {
          debug('**********messageData.data***********')
          debug(messageData)
          console.log('message::::::::::', messageData.data)
          marr.push(messageData.data)
          // marr.push(messageData.data);
          if (procOs === 'ios') {
            if (marr.length >= iosLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) {
                  throw err
                }
              })
              marr = []
            }
          } else {
            if (marr.length >= andLimit) {
              total = total + marr.length
              threads = threads + 1
              authAndGetCol(marr, 0, function (err, aagcrs) {
                if (err) {
                  console.log(err)
                }
              })
              marr = []
            }
          }

          debug('**********messageData.data***********')
          let ackMsg = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
            'to': messageData.from,
            'message_id': messageData.message_id,
            'message_type': 'ack'
          }))

          // send back the ack.
          cl.send(ackMsg)
          debug('Sent ack')

          // Now do something useful here with the message
          // e.g. awesomefunction(messageData);
          // but let's just log it.
          // debug(messageData);
        } else {
          // Need to do something more here for a nack.
          console.log('message was an ack or nack...discarding')
        }
      } else {
        console.error('error')
        console.error(stanza)
      }
    })

    cl.on('offline', function (e) {
      console.error('Client is offline', cl.connection.reconnect, e)
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        }
        clDisconnect()
      })
    })

    cl.on('connect', function () {
      console.log('Client is connected')
    })

    cl.on('reconnect', function () {
      console.error('Client reconnects …')
    })

    cl.on('disconnect', function (e) {
      console.error('Client is disconnected', cl.connection.reconnect, e)
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        }
        clDisconnect()
      })
    })

    cl.on('error', function (e) {
      console.error('Error occured:')
      console.error(e)
      console.error(e.children)
      console.error('reinitiating xmpp server connection')
      total = total + marr.length
      totalsleep = 0
      threads = threads + 1
      authAndGetCol(marr, 0, function (err, aagcrs) {
        if (err) {
          throw err
        } else {
          clDisconnect()
        }
      })
    })
  })
}

function clDisconnect () {
  if ((total === (processed + skipped)) || totalsleep === 180) {
    console.error('client disconnected, exiting process')
    console.error('skipped ', skipped)
    console.error('processed ', processed)
    console.error('total ', total)
    console.error('threads ', threads)
    console.error('respth ', respth)
    console.error('totalsleep ', totalsleep)
    if (mongoConn) {
      mongoConn.close()
    }
    if (pool) {
      pool.end(function (err) {
        if (err) {
          console.log(err)
        } else {
          process.exit(1)
        }
      })
    } else {
      process.exit(1)
    }
  } else {
    sleep(30000)
    totalsleep = totalsleep + 30
    clDisconnect()
  }
}

let authAndGetCol = function (marr, start, aagccb) {
  userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
  daywiseLoans = 'user_daywise_loans_' + dayOfMonth + '_' + month + '_' + year
  if (!mongoConn) {
    connectMongo(function (err, ccrs) {
      if (err) {
        aagccb(err, ccrs)
      } else {
        authAndGetCol(marr, start, aagccb)
      }
    })
  } else {
    parseGCMMessage(marr, start, function (err, pgmrs) {
      aagccb(err, pgmrs)
    })
  }
}

let createIndex = function () {
  console.error('create index called')

  let collection = mongoConn.collection(userDaywiseStats)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_loans')
  collection.createIndex({ date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection(daywiseLoans)
  collection.createIndex({ user_id: 1, used_date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })

  collection = mongoConn.collection('overall_day_stats')
  collection.createIndex({ date: 1, App_Version: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
  collection = mongoConn.collection('unknown_clicks')
  collection.createIndex({ date: 1, App_Version: 1, EventName: 1 }, function (err, indexName) {
    if (err) {
      console.error('error while creating index', err)
    }
  })
}

let parseGCMMessage = function (marr, start, pgmcb) {
  try {
    if (marr[start]) {
      if (marr[start]) {
        let message = marr[start]
        let collection = mongoConn.collection('unknown_clicks')
        if (message.Token !== '') {
          JWT.verify(message.Token, 'W@mon^^7))9eYYY$riniVa$aRR@mAnuJ@NNn', function (err, tokenData) {
            if (err) console.error(err)
            if (tokenData) {
              collection.insertOne({ date: new Date(), App_Version: message.App_Version, mobile_number: message.Number, user_id: tokenData._id, EventName: message.EventName }, function (error, result) {
                if (error) {
                  console.log('errr', error)
                } else {
                  // console.log(result)
                  processed = processed + 1
                }
                parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
              })
            }
          })
        } else {
          collection.insertOne({ date: new Date(), App_Version: message.App_Version, mobile_number: message.Number, user_id: '', EventName: message.EventName }, function (error, result) {
            if (error) {
              console.log('errr', error)
            } else {
              // console.log(result)
              processed = processed + 1
            }
            parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
          })
        }
      } else {
        parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
      }
    } else {
      console.log('completed array')
    }
  } catch (cerr) {
    console.error('unkown error occured in parseGCMMessage')
    console.error(cerr)
    parseGCMMessage(marr, (parseInt(start) + 1), pgmcb)
  }
}
